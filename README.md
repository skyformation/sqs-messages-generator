# HOW TO RUN

0. have nodejs installed on your machine. open a command-line window.
1. clone this repo: `git clone https://grnadav@bitbucket.org/skyformation/sqs-message-generator.git`
2. install dependencies `npm install`
2. generate messages: `node index.js generate-sqs-messages --s3.bucket.name my-bucket --s3.bucket.region us-west-2 --sqs.queue.url https://sqs.us-west-2.amazonaws.com/123456789012/myqueue --sqs.queue.region us-west-2 --key MY_KEY --secret MY_SECRET"`

