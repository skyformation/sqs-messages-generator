'use strict';

const AWS = require('aws-sdk');
const _ = require('lodash');

async function generateSqsMessages({
                                       key,
                                       secret,
                                       limit = undefined,
                                       s3 = {},
                                       sqs = {}
                                   } = {}) {

    let totalGenerated = 0;
    let finished = false;
    for await (let s3Objects of listS3BucketObjects({key, secret, s3})) {

        // take up to <limit> objects
        if (limit !== undefined && (totalGenerated + s3Objects.length > limit)) {
            s3Objects = _.slice(s3Objects, 0, limit - totalGenerated);
            finished = true;
        }
        const responses = await createSqsMessagesForS3Objects({key, secret, s3, sqs, s3Objects});

        console.log(`Generated [${responses.length}] messages in queue`);
        totalGenerated += responses.length;

        // reached limit
        if (finished) {
            break;
        }
    }

    console.log(`DONE! Generated [${totalGenerated}] messages in queue`);
    return totalGenerated;

}

async function* listS3BucketObjects({
                                        key,
                                        secret,
                                        s3 = {}
                                    } = {}) {

    const s3client = new AWS.S3({
        apiVersion: '2006-03-01',
        accessKeyId: key,
        secretAccessKey: secret,
        region: s3.bucket.region
    });

    const listObjectsReqParams = {
        Bucket: s3.bucket.name,
        Prefix: s3.bucket.subfolder || '',
        MaxKeys: 1000 // max the API allows
    };
    // handle pagination if more than 1000 files in bucket

    let data;
    do {
        data = await s3client.listObjectsV2(listObjectsReqParams).promise();
        listObjectsReqParams.ContinuationToken = data.NextContinuationToken;
        // filter out keys that are actually folders, and concat all real objects to final result
        const filtered = _.filter(_.get(data, 'Contents', []), item => !_.endsWith(item.Key, '/'));
        yield filtered;
    } while (data.IsTruncated);

}

async function createSqsMessagesForS3Objects({
                                                 key,
                                                 secret,
                                                 s3,
                                                 sqs,
                                                 s3Objects
                                             }) {

    const sqsClient = new AWS.SQS({
        apiVersion: '2012-11-05',
        accessKeyId: key,
        secretAccessKey: secret,
        region: sqs.queue.region

    });

    const reqs = s3Objects.map(s3Object => {

        const message = {
            "Records": [{
                "eventVersion": "2.0",
                "eventSource": "aws:s3",
                "awsRegion": s3.bucket.region, // i.e. "us-west-2",
                "eventTime": "2017-09-10T09:39:11.064Z",
                "eventName": "ObjectCreated:Put",
                "s3": {
                    "s3SchemaVersion": "1.0",
                    "configurationId": "NewFile",
                    "bucket": {
                        "name": s3.bucket.name, // i.e. "mybucket"
                        "arn": "arn:aws:s3:::" + s3.bucket.name
                    },
                    "object": {
                        "key": s3Object.Key, // i.e. "my-file-1.gz",
                        "size": s3Object.Size, // i.e. 1396657,
                        "eTag": s3Object.ETag // i.e. "a45a6cc3f1723f431bb57157d56fbb50",
                    }
                }
            }]
        };

        const reqParam = {
            MessageBody: JSON.stringify(message),
            QueueUrl: sqs.queue.url
        };

        return sqsClient.sendMessage(reqParam).promise();

    });

    return Promise.all(reqs);

}


module.exports = {
    generateSqsMessages
};