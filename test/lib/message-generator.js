'use strict';

const chai = require('chai');
const expect = chai.expect;

// load params for test from .env file
require('dotenv').config({path: process.cwd() + '/test/lib/.env'});

const messageGenerator = require('../../lib/message-generator.js');

describe('Message generator', function () {

    it('should generate messages', async function () {

        this.timeout(60000);

        const numMessagesToGenerate = 11;
        const totalGenerated = await messageGenerator.generateSqsMessages({
            key: process.env.AWS_KEY,
            secret: process.env.AWS_SECRET,
            s3: {
                bucket: {
                    name: process.env.S3_BUCKET_NAME,
                    region: process.env.S3_BUCKET_REGION
                }
            },
            sqs: {
                queue: {
                    url: process.env.SQS_URL,
                    region: process.env.SQS_REGION
                }
            },
            limit: numMessagesToGenerate
        });

        expect(totalGenerated).to.equal(numMessagesToGenerate);
    });

});