#!/usr/bin/env node
'use strict';

const yargs = require("yargs");
const messageGenerator = require('./lib/message-generator.js');

const argv = yargs.usage("$0 command")
    .command('generate-sqs-messages', 'generate sqs messages for objects in a given bucket', yargsin => {

        yargsin
            .options({
                'key': {
                    type: 'string'
                },
                'secret': {
                    type: 'string'
                },
                's3.bucket.name': {
                    type: 'string'
                },
                's3.bucket.region': {
                    type: 'string'
                },
                's3.bucket.subfolder': {
                    type: 'string',
                    demandOption: false
                },
                'sqs.queue.region': {
                    type: 'string'
                },
                'sqs.queue.url': {
                    type: 'string'
                },
                'limit': {
                    type: 'number',
                    demandOption: false
                }
            })


    }, messageGenerator.generateSqsMessages)

    .demand(1, "must provide a valid command")
    .help("h")
    .alias("h", "help")
    .argv;

